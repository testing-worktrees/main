#!/bin/bash

fetch()
{
	git remote add lettuce git@gitlab.com:testing-worktrees/lettuce.git
	git remote add potatoe git@gitlab.com:testing-worktrees/potatoe.git
	git remote add carrot git@gitlab.com:testing-worktrees/carrot.git

	git fetch lettuce
	git fetch potatoe
	git fetch carrot

	# lettuce and carrot are external apss, we keep our mpi/ompss-2
	# branches in this repository
	git branch --track bench/lettuce/master  lettuce/master
	git branch --track bench/lettuce/mpi     origin/bench/lettuce/mpi
	git branch --track bench/lettuce/ompss-2 origin/bench/lettuce/ompss-2

	git branch --track bench/carrot/master   carrot/master
	git branch --track bench/carrot/mpi      origin/bench/carrot/mpi
	git branch --track bench/carrot/ompss-2  origin/bench/carrot/ompss-2

	# potatoe is a bsc app. It's main repo keeps its own mpi/ompss-2
	# branches, but we have our own copy too.
	git branch --track bench/potatoe/master  potatoe/master
	git branch --track bench/potatoe/mpi     origin/bench/potatoe/mpi
	git branch --track bench/potatoe/ompss-2 origin/bench/potatoe/ompss-2
}

checkout()
{
	mkdir -p potatoe lettuce carrot

	git worktree add ./potatoe/master  bench/potatoe/master
	git worktree add ./potatoe/mpi     bench/potatoe/mpi
	git worktree add ./potatoe/ompss-2 bench/potatoe/ompss-2

	git worktree add ./carrot/master   bench/carrot/master
	git worktree add ./carrot/mpi      bench/carrot/mpi
	git worktree add ./carrot/ompss-2  bench/carrot/ompss-2

	git worktree add ./lettuce/master  bench/lettuce/master
	git worktree add ./lettuce/mpi     bench/lettuce/mpi
	git worktree add ./lettuce/ompss-2 bench/lettuce/ompss-2
}

init()
{
	fetch
	checkout
}

import()
{
	git fetch lettuce
	git fetch potatoe
	git fetch carrot

	git push origin bench/lettuce/master
	git push origin bench/carrot/master

	# potatoe is a bsc app, and we need to import also the mpi and ompss-2
	# branches
	git push origin bench/potatoe/master
	git push origin bench/potatoe/mpi
	git push origin bench/potatoe/ompss-2
}

__update_app()
{
	APP=$1
	git fetch $APP
	cd $APP
	cd master
	git pull
	cd ../mpi
	git pull
	cd ../ompss-2
	cd ../..
}

update()
{
	__update_app potatoe
	__update_app carrot
	__update_app lettuce
}

__reset_app()
{
	APP=$1
	git worktree remove $APP/master
	git worktree remove $APP/ompss-2
	git worktree remove $APP/mpi

	git branch -d bench/$APP/master
	git branch -d bench/$APP/ompss-2
	git branch -d bench/$APP/mpi

	git remote remove $APP
}

reset()
{
	__reset_app potatoe
	__reset_app carrot
	__reset_app lettuce
	rmdir carrot lettuce potatoe
}


CMD=$1

case $CMD in
	init)
		init
		;;
	fetch)
		fetch
		;;
	checkout)
		checkout
		;;
	update)
		update
		;;
	import)
		import
		;;
	reset)
		reset
		;;
	*)
		>&2 echo "Command $CMD not understood"
		exit 1
		;;
esac
